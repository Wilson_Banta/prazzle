var extend = require('util')._extend;
var helper = require('./helper');
var decrypt = require.main.require('lib/decrypt.js');

var validRequest = {
	secretName: 'test-secret',
	environment: 'testing-env',
	s3Bucket: 'test-bucket'
};

describe('decrypt', function() {

	extend(decrypt, helper);

	it('should throw error if secretName not supplied', function() {
		decrypt.should_have_error_when_missing_argument(validRequest, 'secretName');
	});

	it('should throw error if environment not supplied', function() {
		decrypt.should_have_error_when_missing_argument(validRequest, 'environment');
	});

	it('should throw error if s3Bucket not supplied', function() {
		decrypt.should_have_error_when_missing_argument(validRequest, 's3Bucket');
	});

});
