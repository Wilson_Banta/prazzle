var extend = require('util')._extend;
var helper = require('./helper');
var encrypt = require.main.require('lib/encrypt.js');

var validRequest = {
	secretName: 'test-secret',
	plainText: 'test-plain',
	environment: 'testing-env',
	s3Bucket: 'test-bucket',
	kmsKey: 'test-kmsKey'
};

describe('encrypt', function() {

	extend(encrypt, helper);

	it('should throw error if secretName not supplied', function() {
		encrypt.should_have_error_when_missing_argument(validRequest, 'secretName');
	});

	it('should throw error if plainText not supplied', function() {
		encrypt.should_have_error_when_missing_argument(validRequest, 'plainText');
	});

	it('should throw error if environment not supplied', function() {
		encrypt.should_have_error_when_missing_argument(validRequest, 'environment');
	});

	it('should throw error if s3Bucket not supplied', function() {
		encrypt.should_have_error_when_missing_argument(validRequest, 's3Bucket');
	});

	it('should throw error if kmsKey not supplied', function() {
		encrypt.should_have_error_when_missing_argument(validRequest, 'kmsKey');
	});

});
