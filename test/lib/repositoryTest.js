var tmp = require('tmp');
var testBucket = 'prazzle-test';
var repositoryFactory = require("../../lib/utils/repository");
var log = require('winston');

describe('repositoryTest', function() {

	var repository;

	before(function(cb){
		repository = repositoryFactory({s3Bucket: testBucket});

		var tmp = require('tmp');

		tmp.file(function _tempFileCreated(err, path, fd, cleanupCallback) {
			if (err) throw err;

			console.log("File: ", path);

			repository.uploadPackageZip(
				{
					file: path,
					metadata: {
						project: 'test-name',
						version: '1.0.0.0',
						packageType: 'test',

						env: {
							key_1: "",
							key_2: ""
						}
					}
				}, function(e, r){

					if(e) throw e;
					console.log(r);
					cb();
				});
			});

		})

		it('should get config for know package', function(cb) {
			repository.getPackageMetaData( { log: log,	project: 'test-name',	version: '1.0.0.0'  } , function(e,r){
				if(e) throw e;
				console.log('Keys are: %j', r);

				if(r.env.key_1 !== '') throw 'key_1 missing';
				if(r.env.key_2 !== '') throw 'key_1 missing';
				cb();
			});
		});

		it('should set and get env for package', function(cb) {
			repository.setConfig( { 	project: 'test-name',	environment: 'mochaTest', version: '1.0.0', packageType: 'test',  env: { key_1: "key_1_v",	key_2: "key_2_v"} }  , function(e,r){
				if(e) throw e;

				repository.getEnv({ 	project: 'test-name',	environment: 'mochaTest' } , function(err, r){

					if(r.key_1 !== 'key_1_v') throw 'key_1 missing';
					if(r.key_2 !== 'key_2_v') throw 'key_1 missing';
					console.log(r);
					cb();
				})

			});
		});




	});
