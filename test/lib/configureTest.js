var extend = require('util')._extend;
var helper = require('./helper');
var configure = require.main.require('lib/configure.js');

var validRequest = {
	environment: 'testing-env',
	s3Bucket: 'test-bucket'
};

describe('configure', function() {

	extend(configure, helper);

	it('should throw error if environment not supplied and not interactive', function() {
		configure.should_have_error_when_missing_argument(validRequest, 'environment');
	});

	it('should throw error if s3Bucket not supplied and not interactive', function() {
		configure.should_have_error_when_missing_argument(validRequest, 's3Bucket');
	});

});
