var sinon = require('sinon');
var assert = require('better-assert');
var git = require.main.require('lib/utils/gitHelper.js');
var version = require.main.require('lib/version.js');

describe('version dir', function() {
	var stubbed = false;

	function setup(branchName, revisionCount){
		stubbed = true;
		sinon.stub(git, 'branch', function(cb) { return cb(branchName); });
		sinon.stub(git, 'revisionCount', function(branch,cb) { return cb(revisionCount); });
	}

	it('should not include master branch name', function(cb){

		setup('master','45');

		version.run('1.0.0', function(e,v){
			console.log('Result: ' + v);
			assert(v === '1.0.45');
			cb();
		});
	});

	it('should include non master branch name', function(cb){
		setup('develop','45');

		version.run('1.0.0', function(e,v){
			console.log('Result: ' + v);
			assert(v === '1.0.0-develop.45');
			cb();
		});
	});

	it('branch with slash should be replaced by dash', function(cb){
		setup('releases/v3.12','45');

		version.run('1.0.0',function(e,v){
			console.log('Result: ' + v);
			assert(v === '1.0.0-releases-v3-12.45');
			cb();
		});
	});

	beforeEach(function(){
		stubbed = false;
	})

	afterEach(function(){
		if(stubbed){
			git.revisionCount.restore();
			git.branch.restore();
		}
	})

});
