var installer = require('./install');
var async = require('async');
var path = require('path');
var extend = require('util')._extend;
var fs = require('fs');
var zipHelper = require('./utils/zipHelper');
var log = require('winston');
var repository;
var repositoryFactory = require("./utils/repository");
var rimraf = require('rimraf');
var decryptor = require('./decrypt');

exports.run = function(options, cb) {

    if (!options.s3Bucket) throw new Error('s3Bucket must be supplied');
    if (!options.name) throw new Error('name must be supplied');
    if (!options.environment) throw new Error('environment must be supplied');
    if (!options.targetDir) throw new Error('targetDir must be supplied');
    if (!options.packagesDir) throw new Error('packagesDir must be supplied');

    repository = repositoryFactory(options);
    var context = {};
    async.waterfall([
        download_package_definition_for_environment(options, context),
        cb => async.parallel(
            [
                download_package_zip(options, context),
                download_environment_secrets(options, context),
            ], e => cb(e)),
        calculate_install_dir(options, context),
        unzip_package_to_target_directory(options, context),
        run_package_install_process(options, context),
        clean_up_target_dir(options, context),
    ], cb);
}

function download_environment_secrets(options, context) {
    return cb => {

        //option to skip the install
        if (options.skipSecrets) return cb();

        var secrets = {};

        context.secrets = secrets;

        function secretHandler(key) {
            return secrets[key];
        }

        repository.getSecretList(options, function(err, results) {
            results = results || [];
            log.debug('Got List Of Secrets, count: ' + results.join(', '));
            async.each(results, function(secret, callback) {

                    if (!secret) return callback();

                    decryptor.run({
                        secretName: secret,
                        environment: options.environment,
                        s3Bucket: options.s3Bucket,
                        kmsRegion: options.kmsRegion || 'ap-southeast-2'
                    }, (e, r) => {
                        secrets[secret] = r;
                        callback();
                    });
                },
                (e) => cb(e));
        });
    }
}


function download_package_definition_for_environment(options, context) {
    return (cb) => {
        return repository.getConfig({
            project: options.name,
            environment: options.environment
        }, function(err, r) {
            if (err) return cb(err);
            if (!r) throw `No package config could be download for ${options.name} - environment: ${options.environment}`;
            context.config = r;
            log.verbose('Config downloaded:', options);
            log.debug('Config is: ', r);
            cb();
        });
    };
}

function clean_up_target_dir(options, context) {
    return cb => {

        fs.readdirSync(options.targetDir).filter(function(file) {
            if (fs.statSync(path.join(options.targetDir, file)).isDirectory()) {
                if (file.match(/^[0-9]{8}-[0-9]{6}$/) && file !== context.installDirName) {
                    log.verbose('Cleaning up old installation direcotry: ' + file);
                    try {
                        rimraf.sync(path.join(options.targetDir, file));
                    } catch (e) {
                      console.log(e);
                      //not to worry we'll clean it up next time (hopefully)
                    }
                }
            }
        });

        cb();
    }
}


function calculate_install_dir(options, context) {
    return cb => {
        //check for a string or a number,
        if (options.noTimeBasedSubDirectory && (options.noTimeBasedSubDirectory === 1 || options.noTimeBasedSubDirectory === "1" ||
                options.noTimeBasedSubDirectory === "Y")) {
            context.installDirName = options.name;
            context.installDirPath = options.targetDir;
        } else {
            context.installDirName = getDateTime();
            context.installDirPath = path.join(options.targetDir, context.installDirName);
        }
        cb();
    }
}


function download_package_zip(options, context) {
    return (cb) => {

        var params = {
            name: options.name,
            version: context.config.version,
            zipFilename: path.join(options.packagesDir, context.config.filename),
            md5: context.config.md5,
            filename: context.config.filename,
        };

        log.verbose('Downloading package zip:', params);

        repository.downloadPackageZip(params, (err) => {
            if (err) return cb(err);
            context.downloadedZip = params.zipFilename;
            log.verbose('Download complete: ', params.zipFilename);
            cb()
        });
    };
}

function unzip_package_to_target_directory(options, context) {
    return (cb) => {

        log.verbose('Unzipping package zip:', options);

        var zip = context.downloadedZip;

        zipHelper.unZip(zip, context.installDirPath, cb);
    };
}

var variableExpander = require('./utils/variableExpander')

function run_package_install_process(options, context) {
    return (cb) => {


        //option to skip the install
        if (options.skipInstall) return cb();

        log.verbose(`Running Installer, name: ${options.name}, version: ${context.config.version}, packageType: ${context.config.packageType}, installDirPath: ${context.installDirPath} `);

        var cwd = process.cwd();

        process.chdir(context.installDirPath);

        //Specify / Overwrite the environment.
        context.config.environment = options.environment;

        //Resolve any secrets.
        if (context.config.env) {
            context.config.env.EMPTY = '';
            context.config.env.PRAZZLE_ENV = options.environment;
            context.config.env.PRAZZLE_ENV_NAME = options.environment;

            context.config.env = variableExpander.resolver(context.config.env, s => context.secrets[s]).toHash();
        }

        var installOptions = Object.assign({}, context.config, options);

        try {
            installer.run(installOptions, function(err, r) {
                process.chdir(cwd);
                cb(err);
            });
        } catch (e) {
            process.chdir(cwd);
            cb(e);
        }

    };
}



function getDateTime() {

    var date = new Date();

    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;

    var min = date.getMinutes();
    min = (min < 10 ? "0" : "") + min;

    var sec = date.getSeconds();
    sec = (sec < 10 ? "0" : "") + sec;

    var year = date.getFullYear();

    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;

    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;

    return year + "" + month + "" + day + "-" + hour + "" + min + "" + sec;

}
