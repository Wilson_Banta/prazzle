var path = require('path');
var fs = require('fs');
var log = require('winston');

var git = require('./gitHelper.js');

module.exports = function(versionBase, cb){
	console.log(versionBase);
	git.branch(function(branchName){

		branchName = process.env.GIT_BRANCH_NAME || branchName;

		git.revisionCount(branchName, function(revisionCount){

			if(!revisionCount) return  cb(null,versionBase );

			const buildNumber = process.env.CODEBUILD_BUILD_NUMBER || revisionCount;

			var versionSplit = versionBase.split('.');

			var branchNameNoSlash = branchName.replace(/[^A-Za-z0-9]/g,'-');

			var buildVersion = branchName == 'master' ?
							versionSplit[0] + '.' + versionSplit[1] + '.' + buildNumber :
							versionSplit[0] + '.' + versionSplit[1] + '.' + (versionSplit[2] || 0 ) + '-' + branchNameNoSlash + '.' + buildNumber;

			cb(null, buildVersion);
		});
	});
}
