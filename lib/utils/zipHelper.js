var fs = require('fs');
var path = require('path');
var md5Helper = require('./md5Helper');
var mkdirp = require('mkdirp');
var log = require('winston');
var streams = require('memory-streams');
var rimraf = require('rimraf');

exports.zipDir = function(dir, outputFilename, callback) {

	mkdirp.sync(path.dirname(outputFilename));

	output = fs.createWriteStream(outputFilename),
	archive = require('archiver')('zip', { zlib: {level: 9} });

	log.debug('Creating zip file... packageTempDir: %s', dir);

	output.on('close', function() {
		log.debug('%s created: %s total bytes', outputFilename, archive.pointer());
		callback();
	});

	archive.on('error', function(err) {
		log.error(err);
	});

	archive.pipe(output);
	archive.directory(dir, '').finalize();
}

var unzip2 = require('unzip2');

exports.unZip = function(source, target, callback) {
//	return unzipMissing(source, target, callback);

	log.debug('unZip soruce:', source, 'target:' , target);

	if(!fs.existsSync(source)) throw 'Source missing: ' + source;

	if(fs.existsSync(target)) {
		rimraf.sync(target);
	}

	mkdirp.sync(target);


	fs.createReadStream(source)
		.pipe(unzip2.Extract({ path: target }))
		.on('error', function(err) {
			log.error(err);
		})
		.on('close', function() {
			log.debug('%s extracted', source);
			callback();
		});
}

function ensureDirExists(f){
	var dir = path.dirname(f);
	if(!fs.existsSync(dir)) mkdirp.sync(dir);
}

function unzipMissing(zip, outputDir, done)
{
    fs.createReadStream(zip)
      .pipe(unzip2.Parse())
      .on("entry", function(entry) {

		  if(entry.type == 'Directory'){
			    entry.autodrain();
				return;
		  }

          var targetFilePath = path.join(outputDir, entry.path).replace(/\\/g,"/");

		  ensureDirExists(targetFilePath);

          if(fs.existsSync(targetFilePath))
          {
                var zipHash = null;
                var existingHash = null;

                md5Helper.fromFile(targetFilePath, function(existingHash){
                    var ms  = new streams.WritableStream();
                    md5Helper.fromStream(entry, ms, function(zipHash){

                        if(existingHash === zipHash){
                            return;
                        }
                        fs.writeFileSync(targetFilePath, ms);
                    })
                })
          }
          else{
               entry.pipe(fs.createWriteStream(targetFilePath));
          }
      })
      .on("close", function() {
          done();
      });
}
