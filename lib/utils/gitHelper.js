
var exec = require('child_process').exec

function _command (cmd, cb) {
  exec(cmd, { cwd: process.cwd() }, function (err, stdout, stderr) {
    cb(stdout.split('\n').join(''))
  })
}

module.exports = {
	revisionCount: function(commitId, cb){
		_command('git rev-list --count ' + commitId, cb)
	},
	clean : function(cb){
		_command('git clean -f -x -d -e node_modules -e.env', function(r){ console.log(r);})
	},
    short : function (cb) {
      _command('git rev-parse --short HEAD', cb)
    }
  , long : function (cb) {
      _command('git rev-parse HEAD', cb)
    }
  , branch : function (cb) {
	  if(process.env.GIT_BRANCH) return cb(process.env.GIT_BRANCH );

      _command('git rev-parse --abbrev-ref HEAD', cb)
    }
  , tag : function (cb) {
      _command('git describe --always --tag --abbrev=0', cb)
    }
  , log : function (cb) {
      _command('git log --no-color --pretty=format:\'[ "%H", "%s", "%cr", "%an" ],\' --abbrev-commit', function (str) {
        str = str.substr(0, str.length-1)
        cb(JSON.parse('[' + str + ']'))
      })
    }
}
