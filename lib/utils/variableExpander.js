var log = require('winston');
var secretFinderRegex = /\${secret:([^}]*)}/gi;

exports.getSecrets = function(value){

	var results = [];

	log.debug('looking for secrets in :', value);
	var matches = getMatches(secretFinderRegex, value);

	log.debug('Matches:', matches);

	if(!matches) return [];
	for(var i =0; i < matches.length; i++)
	{
		var m = matches[i];
		results.push(m[1]);
	}

	return results;
}

function getMatches(regex, text){

	var matches = [], found;
	regex.lastIndex = null;
	while (found = regex.exec(text)) {
		matches.push(found);
		regex.lastIndex = found.index+1;
	}
	return matches;
}







exports.resolver = function(settings, secretHandler)
{

	var regex = /\${([^}]*)}/gi;

	function _getSetting(key, alreadyLookup)
	{
		var setting = settings[key]; // ? settings[key] : null;

		if (setting)
			return _resolve(setting, alreadyLookup);

		return setting;
	}

	function _setting(key, alreadyLookup)
	{
		var raw = _getSetting(key, alreadyLookup);
		return _resolve(raw, alreadyLookup);
	}


	function _resolve(input, alreadyLookup)
	{
		if (!input) return input;

		if (input.indexOf('{') < 0) return input;

		var matches = [], found;
		regex.lastIndex = null;
		while (found = regex.exec(input)) {
			matches.push(found);
			regex.lastIndex = found.index+1;
		}

		for(var i =0; i < matches.length; i++)
		{
			var m = matches[i];

			var token = m[0];
			var tokenKey = m[1];

			if(alreadyLookup[tokenKey])
				throw "Self referencing setting detected: "+ tokenKey;

			alreadyLookup[tokenKey] = true;;

			var resolved = secretHandler && tokenKey.indexOf("secret:") === 0 ?
			secretHandler(tokenKey.substring("secret:".length))  :
			_setting(tokenKey, alreadyLookup);

			if (resolved == null){
				throw new Error("The token: \""+ token + "\" failed to resolve to an appSetting value");
			}

			input = input.replace(token, resolved);

			alreadyLookup[tokenKey] = null;
		}

		return input;
	}



	return {

		getSetting: function(key)
		{
			var setting = settings[key] ? settings[key] : null;
			if (setting != null)
			return _resolve(setting, {});
			return null;
		}
		,
		resolve: function(text)
		{
			return _resolve(text, {});
		}
		,
		addSetting: function(key,value)
		{
			settings[key] = value;
		},
		hasTokens: function(text)
		{
			if (!text) return false;
			return regex.test(text);
		},
		resolveSettings: function()
		{
			var cantResolve = [];
			for (var i = 0; i < 50; i++)
			{
				var tempSettings = {};

				for(var key in settings)
				{
					tempSettings[key] = _resolve(settings[key], {});
				}

				for(var key in tempSettings)
				{
					settings[key] = tempSettings[key];
				}

				cantResolve = [];

				for(var key in settings)
				{
					if (this.hasTokens(settings[key]) )
					{
						cantResolve.push(key + ":" + settings[key]);
					}
				}

				if (cantResolve.length == 0)
				return null;
			}

			return cantResolve;
		},
		toHash: function()
		{
			var result = {};

			for(var k in settings)
			{
				result[k] = this.resolve(settings[k]);
			}
			return result;
		}
	};
};
