var fs = require('fs');
var mkdirp = require('mkdirp');
var rimraf = require('rimraf');

module.exports = function(dir, cb){

	if(!fs.existsSync(dir)){
		mkdirp(dir, cb);
	}

	rimraf(dir, function(e, r){

		if(e) return cb(e);

		for(var i=0; i < 10; i ++){
			try{
				fs.mkdirSync(dir);
				return setTimeout(function(){
					return cb(null);
				}, 300);
			}
			catch(e){
				if(i == 9)
					throw e;
			}

		}
	});
}
