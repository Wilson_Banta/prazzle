var crypto = require('crypto');
var fs = require('fs');

module.exports.fromFile = function(filename, callback)
{
    var md5sum = crypto.createHash('md5');

    var s = fs.ReadStream(filename);
    s.on('data', function(d) {
      md5sum.update(d);
    });

    s.on('end', function() {
      var d = urlEncode(md5sum.digest('base64'));
      callback(d);
    });
}

module.exports.fromStream = function (s, output, callback){

        var md5sum = crypto.createHash('md5');

        s.on('data', function(d) {
            output.write(d);
            md5sum.update(d);
        });

        s.on('end', function() {
          var d = urlEncode(md5sum.digest('base64'));
          callback(d);
        });
}

function urlEncode(unencoded) {
  return unencoded.replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');
};
