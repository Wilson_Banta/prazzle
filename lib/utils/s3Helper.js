var AWS = require('aws-sdk');
var path = require('path');
var fs = require('fs');
var assert = require('assert');

exports.upload = function(options, callback) {

	assert('object' == typeof options);
	assert('string' == typeof options.keyBase);
	assert('string' == typeof options.name);
	assert('string' == typeof options.version);
	assert('string' == typeof options.bucket);
	assert('string' == typeof options.bucket);

	if(options.metadata){
		for(var key in options.metadata){
			options.metadata[key] = options.metadata[key] || '';
		}
	}

	var s3 = new AWS.S3(),
		extname =  path.extname(options.file),
		key =  [options.keyBase, options.name, options.version + extname].join('/'),
		params = {
			Bucket: options.bucket,
			Key: key ,
			Body: fs.createReadStream(options.file),
			Metadata: options.metadata
	};

	s3.upload(params, callback);
}
