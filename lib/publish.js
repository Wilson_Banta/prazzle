var assert = require('better-assert');
var async = require('async');
var pluginLoader = require('./utils/packagePluginLoader');
var extend = require('util')._extend;
var path = require('path');
var walk = require('walk');
var mkdirp = require('mkdirp');
var fs = require('fs');
var repositoryFactory = require("./utils/repository");
var packager = require('./package');
var log = require('winston');
var cleanDir = require('./utils/cleanDir');

var repository, results;
var outputDir;

exports.run = function(options, publishDoneCb) {

    assert(options);

    outputDir = options.outputDir || path.join('..', 'dist');

    repository = repositoryFactory(options);

    async.waterfall([
        cleanIfFlagged(options),
        collectResults,
        function(results, next) {
            runPublishIfNoResults(results, options, next)
        },
        collectResults,
        function(results, next) {
            saveResults(results, options, next)
        },
    ], function(err, results) {
        log.info('Publish Complete. %s items', results.length);
        if (publishDoneCb) publishDoneCb();
    })

}

function cleanIfFlagged(options) {
    return cb => {
        mkdirp(outputDir, function() {
            if (options.clean) {
                return cleanDir(outputDir, function(err, r) {
                    if (err) throw err;
                    return cb();
                });
            }
            return cb();
        });
    }
}


function saveResults(results, options, next) {

    log.info('Starting Package Upload. Result Count: ' + results.length);

    async.forEachOf(results, function(result, key, cb) {

            var uploadOptions = extend(options, result);
            var metadata = result.metadata;
            
            saveVersionToFile(uploadOptions, function(err) { 
                if(err) throw err;
            });

            if (metadata && metadata.deployment == 'server') {
                log.info('Uploading: ' + result.file);
                return repository.uploadPackageZip(uploadOptions, cb);
            }

            if (metadata && metadata.deployment == 'browser') {
                log.info('Uploading: ' + result.file);
                return repository.uploadBrowser(uploadOptions, cb);
            }

            throw 'Unknown key return in publish map: ' + JSON.stringify(result);
        },
        function(err) {
            if (err) throw err;
            log.info('Publish Complete done');
            next(null, results.length);
        });

}

function runPublishIfNoResults(results, options, next) {
    if (results.length > 0) {
        log.info('Package results already exist');
        return next();
    }

    log.info('No Package results exist! running package command');
    return packager.run(options, function() {
        next();
    });
}

function collectResults(cb) {


    if (!fs.existsSync(outputDir)) {
        log.info('Output dir does not exist:' + outputDir)
        return cb(null, []);
    }

    var walker = walk.walk(outputDir);

    var results = [];

    walker.on("file", function(root, fileStats, next) {

        if (path.extname(fileStats.name) === '.json') {
            var packageName = path.basename(fileStats.name, '.prazzle.json');
            var packageNamePath = path.join(root, packageName);
            if (
                //Make sure the 2 files are different. 1 package, 1 package.prazzle.json
                packageName !== fileStats.name && fs.existsSync(packageNamePath)) {

                results.push({
                    file: packageNamePath,
                    metadata: require(path.resolve(path.join(root, fileStats.name)))
                });
            }
        }
        next();
    });

    walker.on("errors", function(root, nodeStatsArray, next) {
        next('Error walking directory for results');
    });

    walker.on("end", function() {
        cb(null, results);
    });
}

function saveVersionToFile (options, cb){    
    var versionFile = options.versionFile || "version.txt";
    fs.writeFile(versionFile, options.version || options.metadata.version, function(err){
        if(err)
            cb(err, options);
        cb(null, options);
    });
}

