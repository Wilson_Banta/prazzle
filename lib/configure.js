var pluginLoader = require('./utils/packagePluginLoader');
var log = require('winston');
var assert = require('better-assert');
var prompt = require('prompt');

exports.run = function(options) {

	assert(options);

	var environment = options.environment,
		s3Bucket = options.s3Bucket;


	log.info('Configure... (s3 bucket: "%s", env: "%s")', s3Bucket, environment);

	var envHelper = require('./utils/envHelper');

	var plugin = pluginLoader.load(options.packageType);

	pluginLoader.configure(options);

	plugin.configure.get(options, function(err,result){
		if(err) throw err;

		var properties = {};

		for(var k in result)
		{
			properties[k] = {
				required: true,
				default: result[k],
			};
		}

		prompt.start();

		prompt.get({properties: properties } , function (err, updated) {
			options.env = updated;
			plugin.configure.set(options);
		});

	});



}
