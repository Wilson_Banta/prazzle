var AWS = require('aws-sdk');
var ini = require('ini');
var fs = require('fs');
var path = require('path');
var log = require('winston');
var yargs = require('yargs');
var extend = require('util')._extend;

function configureAws(cb) {
    var awsConfigFile = path.join(process.env.HOME || process.env.USERPROFILE, '.aws', 'config');

    if (!fs.existsSync(awsConfigFile)) {
        return cb();
    }

    log.info('Found aws config file found : ' + awsConfigFile);
    var awsConfig = fs.readFileSync(awsConfigFile).toString();
    var config = ini.parse(awsConfig)
    var profileConfigKey = 'profile ' + process.env.AWS_ASSUME_PROFILE;
    var profile = config[profileConfigKey];

    if (!profile) {
        log.warn('Profile not found in config: ' + process.env.AWS_ASSUME_PROFILE);
        return cb();
    }

    if (!profile.source_profile) return cb();
    if (!profile.role_arn) return cb();

    var sts = new AWS.STS({
        credentials: new AWS.SharedIniFileCredentials({
            profile: profile.source_profile
        })
    });

    var temp = new AWS.TemporaryCredentials({
        RoleArn: profile.role_arn,
        RoleSessionName: 'prazzle-hostname-' + require("os").hostname()

    });

    AWS.config.credentials = temp;

    temp.refresh(function() {
        log.info('Assumed Role: ' + profile.role_arn);
        cb();
    });

}


exports.run = function(args) {
    configureAws(function() {
        AWS.config.getCredentials(function() {
            parseArgs(args)
        });
    });
}

function parseArgs(args) {

    var prazzleEnv = {},
        currentDirPrazzleEnv = {};

    //check for prazzle.env in dir and 3 above
    if (fs.existsSync('../../../prazzle.json')) prazzleEnv = require(path.resolve('../../../prazzle.json'));
    if (fs.existsSync('../../prazzle.json')) prazzleEnv = extend(prazzleEnv, require(path.resolve('../../prazzle.json')));
    if (fs.existsSync('../prazzle.json')) prazzleEnv = extend(prazzleEnv, require(path.resolve('../prazzle.json')));
    if (fs.existsSync('./prazzle.json')) currentDirPrazzleEnv = require(path.resolve('./prazzle.json'));

    if (currentDirPrazzleEnv.name) currentDirPrazzleEnv.project = currentDirPrazzleEnv.project || currentDirPrazzleEnv.name;

    prazzleEnv = extend(prazzleEnv, currentDirPrazzleEnv);

    var commandHelp = {
        'package': 'Packages the current project ready for distribution',
        'publish': 'Publishes the result of the packge command',
        'configure': 'Writes environment specific configuration to an .env file (from s3)',
        'install': 'Runs the specific install scripts for this package type',
        'deploy': 'Unzips a packaged project into the target directory (copiy the .env file if present). Then run the install command',
        'encrypt': 'Encrypt a environment secret aws kms',
        'decrypt': 'Decrypt a environment secret using aws kms',
        'set-config': 'Sets the configuration'
    };

    var describeHelp = {
        'packageType': 'The type of the package. Can also be set inside the package.json file',
        's3Bucket': 'The name of the s3 bucket used to store the package. Can also be set inside a prazzle.json file',
        'environment': 'The environment used to configure the project. Can also be set inside a prazzle.json file',
        'directory': 'The directory where the package will be deployed. Can also be set inside a prazzle.json file',
        'outputDir': 'The directory to store the results',
        'name': 'The name of the package to deploy',
        'secretName': 'The name of the secret, e.g. XYZ_BILLING_PASSWORD',
        'plainText': 'The sensitive text that needs to be encrypted',
        'kmsKey': 'The kms key to use for encryption. Can also be set inside a prazzle.json file',
        'project': 'Name of the project. Can also be set inside a prazzle.json file',
        'version': 'Version of the package. Can also be set inside a prazzle.json file',
        'key': 'Optional key to set value for (otherwise loops all keys if not provided)',
        'clean': 'Cleans all files based on gitignore, except any .env file',
        'kmsRegion': 'The kms region to use for encryption and decryption',
        'source': 'The source zip file to be deployed.',
        'target': 'The target directory the project should be deployed to',
    };

    var mainArgs = yargs(args)
        .usage('prazzle <command>')
        .command('package', commandHelp.package)
        .command('publish', commandHelp.publish)
        .command('configure', commandHelp.configure)
        .command('install', commandHelp.install)
        .command('deploy', commandHelp.deploy)
        .command('encrypt', commandHelp.encrypt)
        .command('decrypt', commandHelp.decrypt)
        .command('set-config', commandHelp['set-config'])
        .command('clean', commandHelp['clean'])
        .default('log-level', 'info')
        .choices('log-level', ['trace', 'debug', 'info', 'warn', 'error', 'fatal'])
        .demand(1, 'must provide a valid command');

    var command = mainArgs.argv._[0];

    //Set log level
    var logLevel = mainArgs.argv['log-level'];

    if (logLevel != 'trace' && logLevel != 'debug' && logLevel != 'info' && logLevel != 'warn' && logLevel != 'error' && logLevel != 'fatal' && logLevel != 'verbose') logLevel = 'info';

    log.level = logLevel;

    var initArgs = function(options, command, keys, required) {

        options.isWinOS = /^win/.test(process.platform);
        options.log_level = logLevel;

        mainArgs.reset(); //reset and start parsing again.

        var required = required || [];

        var keyDescriptions = {};

        keys.forEach(function(key) {
            keyDescriptions[key] = describeHelp[key];
        })

        var packageArgs = yargs
            .usage('\n* ' + commandHelp[command])
            .array('tags')
            .array('environments')
            .describe(keyDescriptions)
            .help();

        if (required) packageArgs.demand(required);

        var args = packageArgs.argv;


        //packageOptions = readPackageJson(keys);

        var showHelp = false;

        for(var k in args){
          options[k] = args[k];
        }
        
        keys.forEach(function(key) {

            if (args[key] || prazzleEnv[key])
                options[key] = args[key] || prazzleEnv[key];

            if (!options[key] && required.indexOf(key) >= 0) {
                console.warn('Missing required property for: ' + key);
                showHelp = true;
                process.exit();
            }
        })



        if (showHelp) {
            packageArgs.showHelp();
        }

        return options;
    };



    switch (command) {
        case 'package':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'package', ['project', 'packageType', 'clean', 'outputDir']);

            require('./package.js').run(options);
            return;
        case 'publish':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'publish', ['project', 'packageType', 's3Bucket', 's3BucketPublic', 'clean', 'outputDir']);
            require('./publish.js').run(options);
            return;
        case 'configure':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'configure', ['packageType']);
            require('./configure.js').run(options);
            return;
        case 'install':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'install', ['packageType', 'project', 'target']);
            require('./install.js').run(options);
            return;
        case 'deploy':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'deploy', ['source', 'target', 'packageType']);
            require('./deploy.js').run(options);
            return;
        case 'deploy-package':
            var options = currentDirPrazzleEnv;
            initArgs(options, 'deploy-package', ['name', 'environment', 'targetDir', 's3Bucket', 'packagesDir' , 'noTimeBasedSubDirectory']);
            require('./deploy-package.js').run(options, resultHandler);
            return;
        case 'encrypt':
            var options = {};
            initArgs(options, 'encrypt', ['secretName', 'plainText', 'environment', 's3Bucket', 'kmsKey', 'kmsRegion'], ['secretName', 'plainText']);
            require('./encrypt.js').run(options);
            return;
        case 'decrypt':
            var options = {};
            initArgs(options, 'decrypt', ['secretName', 'environment', 's3Bucket', 'kmsRegion'], ['secretName']);
            require('./decrypt.js').run(options);
            return;
        case 'clean':
            var options = {};
            initArgs(options, 'clean', [], []);
            require('./clean.js').run(options, function(v) {
                console.log(v);
            });
            return;
        case 'set-config':
            var options = {};
            initArgs(options, 'set-config', ['environment', 'project', 'version', 's3Bucket', 'mode', 'tag', 'remove']);
            require('./set-config.js').run(options);
            return;
        case 'create-specification':
            var options = {};
            initArgs(options, 'create-specification', ['environment', 's3Bucket']);
            require('./create-specification.js').run(options, resultHandler);
            return;
        case 'deploy-specification':
            var options = {};
            initArgs(options, 'deploy-specification', ['environment', 'environments', 'target', 's3Bucket', 'tags', 'project', 'name']);
            require('./deploy-specification.js').run(options);
            return;
        default:
            console.log('Welcome to prazzle');
            yargs.showHelp();
    };
}

function resultHandler(err){
  if(err) {
    console.log(err);
    throw err;
  }
  log.info('Completed Successfullly');
}
