var pluginLoader = require('./utils/packagePluginLoader');
var assert = require('better-assert');
var extend = require('util')._extend;
var log = require('winston');

exports.run = function(options, cb) {

	assert(options);

	var packageType = options.packageType	;

	assert('string' == typeof packageType);
	assert('object' == typeof log);

	options.isWinOS = /^win/.test(process.platform);

	var plugin = pluginLoader.load(options.packageType);

	pluginLoader.configure(options);

	if(options.env){

		var variables = extend(options.env, {
			EMPTY: '',
			PRAZZLE_ENV: options.environment || 'Default',
			PRAZZLE_ENV_NAME: options.environment || 'Default',
		});

		options.fn.variableExpander.resolver(variables, function(){ throw 'Secret handler not implemented'}).resolveSettings();

		options.env = variables;

	}


	plugin.install(options, function(err)
	{
		if(err) {
			if(cb) return cb(err);
			throw err;
		}
		log.info('Install Complete (name: "%s", type: "%s")', options.project, packageType);
		if(cb) cb();
	});
}
