var assert = require('better-assert');
var repositoryFactory = require("./utils/repository");
var prompt = require('prompt');
var extend = require('util')._extend;
var async = require('async');
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
var log = require('winston');
var diff = require('deep-diff').diff;

var repository, envKeys, envDefaults, tags, name;


exports.run = function(options, cb) {

    var environments = options.environments || [];

    if (options.environment) {
        environments.push(options.environment);
    }

    if (environments.length == 0)
        throw 'Must specify at least 1 environment.'

    async.eachSeries(environments, function iteratee(p, callback) {
        var params = extend( {
            environment: p
        },options);

        exports.runOne(params, callback);

    }, function(err, r) {
        if (err) {
            console.log(err);
            if (cb) return cb(err);
            throw err;
        }
        log.info("Deployments complete. (", environments.join(', '), ')');
        if (cb) return cb(null, r);
    });

}

exports.runOne = function(options, callback) {

    assert(options);

    var environment = options.environment,
        s3Bucket = options.s3Bucket,
        targetDir = options.target;
    tags = options.tags;
    name = options.project || options.name;



    assert('string' == typeof environment);
    assert('string' == typeof s3Bucket);
    assert('string' == typeof targetDir);


    repository = repositoryFactory(options);

    options.packagesDir = path.join(targetDir, '_packages');
    options.deploymentDir = path.join(targetDir, environment);

    mkdirp.sync(options.packagesDir);

    log.info('Deploying Specification (tags: %j) to: %s ... (env: "%s", s3 bucket: "%s")', tags, targetDir, environment, s3Bucket);


    async.waterfall([
        //Download Spec File.
        function(cb) {
            downloadSpec(options, cb);
        },

        //Download All Zips,
        downloadPackages,

        //Install each package (unzip, write env, run installer)
        installPackages

    ], function(err) {
        if (err) throw err;
        log.info("Deployment for", environment, 'complete.');

				if(callback) callback();
    });
}

function downloadSpec(options, cb) {
    return repository.getSpec(options, function(err, r) {
        if (err) return cb(err);
        options.spec = r;
        log.debug('Spec Downloaded.', r.projects.length, 'project(s).');
        cb(null, options);
    });
}

function downloadPackages(options, cb) {
    async.eachSeries(options.spec.projects, function iteratee(p, callback) {

        if (tags && tags.indexOf(p.tag) < 0) return callback();

        p.zipFilename = path.join(options.packagesDir, p.name + '-' + p.version + '.zip');
        var params = extend({
            deploymentDir: options.deploymentDir
        }, p);

        repository.downloadPackageZip(params, callback);
    }, function(err) {
        if (err) return cb(err);

        cb(null, options);
    });
}

function installPackages(options, cb) {


    async.eachSeries(options.spec.projects, function iteratee(item, callback) {

        if (name && name !== item.name) return callback();
        if (tags && tags.indexOf(item.tag) < 0) return callback();

        var newOptions = {
            item: item,
						itemJson: JSON.stringify(item)
        };
        newOptions.packageBaseDir = path.join(options.deploymentDir, item.name);
        newOptions.packageVersionDir = path.join(newOptions.packageBaseDir, path.basename(item.filename, item.extension));
        newOptions.currentPackageSpecFilePath = path.join(newOptions.packageBaseDir, 'currentSpec.json');


        var previousSpec;
        var previousSpecContent;

        if (fs.existsSync(newOptions.currentPackageSpecFilePath)) {
            previousSpecContent = fs.readFileSync(newOptions.currentPackageSpecFilePath).toString();
            previousSpec = JSON.parse(previousSpecContent);
        }

        if (previousSpecContent == JSON.stringify(item)) {
            log.info(`${item.name} is already installed (spec match)`);
            return callback(null, options);
        } else {
            log.verbose('Specs Differ: ');
						log.verbose(diff(previousSpec, item));
        }

        async.waterfall([
            function(cb2) {
                unZipPackage(newOptions, cb2)
            },
            resolveEnvSecrets,
            runInstaller,
            writePackageSpec
        ], function(err) {
            if (err) {
                console.log(err);
                throw err;
            }
            callback();
        });

    }, function(err) {
        if (err) return cb(err);
        cb(null, options);
    });
}

function writePackageSpec(options, cb) {
		console.log('Writing Package Spec: ' + options.item.name);
    fs.writeFileSync(options.currentPackageSpecFilePath, options.itemJson);
    cb(null, options);
}

var zipHelper = require('./utils/zipHelper');

function unZipPackage(options, cb) {

    if (fs.existsSync(options.packageVersionDir)) {
        log.verbose('Unzipping package:' + options.item.zipFilename);
        log.verbose('Skip unzip, folder already exists' + options.packageVersionDir);
        return cb(null, options)
    }

    log.verbose('Unzipping package:' + options.item.zipFilename);

    mkdirp.sync(options.packageVersionDir);

    zipHelper.unZip(options.item.zipFilename, options.packageVersionDir, function() {
        cb(null, options);
    });

}

function resolveEnvSecrets(options, cb) {
    log.verbose('Resolve Env Secrets:', options.item.env);
    cb(null, options);
}

var installer = require('./install');

function runInstaller(options, cb) {
    log.verbose('Running Installer:' + options.item.packageType);
    log.debug(options.item.env);

    var cwd = process.cwd();
    process.chdir(options.packageVersionDir);
    options.item.project = options.item.name;
    try {
        installer.run(extend({log_level: log.level}, options.item), function(err, r) {
            process.chdir(cwd);
            cb(null, options);
        });
    } catch (e) {
        process.chdir(cwd);
        cb(e);
    }


}
