# Prazzle #

The prazzle command utility is used to support packaging, configuring and install software.

### How do I get set up? ###

To get the prazzle command installed onto you machine run the following command.

```
npm install git+ssh://git@bitbucket.org:stickytickets/prazzle.git -g
```

Once install you can run *prazzle* to get detailed information about each command.

```
prazzle
```

### Developing ###

If your making changes to this project and want to test them out locally you can use npm link.

First make sure you uninstall prazzle from the global package repository

```
	npm rm --global prazzle
```

Then go into the prazzle project directory (the working directory where you'll be make the changes) and run npm link
```
 npm link
```

This should setup the link from the global repository to the current directory, you can verify by running
```
npm ls --global prazzle
```
